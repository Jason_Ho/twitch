let nowIndex = 0;
let isLoading = false;

function getdata(cb) {

  var client_id = "41dreqplezn5vnlopx9oq778lty25y";
  var limit = 10;
  isLoading = true;
  var apiurl = "https://api.twitch.tv/kraken/streams/?client_id=" + client_id + "&game=League%20of%20Legends&limit=" + limit + "&offset=" + nowIndex

  $.ajax({
    url: apiurl,
    success: function (response) {
      console.log(response)
      cb(null, response)
    }
  })

}

function appendData() {
  getdata((err, data) => {
    const streams = data.streams;
    const $row = $(".row");
    for (let stream of streams) {
      $row.append(getcolumn(stream));
    }
    nowIndex += 10;
    isLoading = false;

  })

}


$(document).ready(function () {
  appendData();
  $(window).scroll(function () {
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
      if (!isLoading) {
        console.log($(window).scrollTop(),$(window).height(),$(document).height())
        appendData();
      }

    }
  })
})


//return 每個col 的 html
function getcolumn(data) {
  return `
<div class="col">
  <div class="preview">
    <img src="${data.preview.medium}" onload="this.style.opacity=1">
    <div class="placehoder"></div>
  </div>
  <div class="bottom ">
    <div class="logo">
      <img src="${data.channel.logo}" >
    </div>
    <div class="intro">
      <h3 class="title">${data.channel.status}</h3>
      <h3 class="Desc">${data.channel.display_name}</h3>
    </div>
  </div>
</div>
  `

}